FROM node:8-alpine

COPY . /usr/src/app/

RUN cd /usr/src/app/ && \
    yarn install
